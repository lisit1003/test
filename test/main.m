//
//  main.m
//  test
//
//  Created by mac on 14-5-16.
//  Copyright (c) 2014年 hengtian. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
