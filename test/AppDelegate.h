//
//  AppDelegate.h
//  test
//
//  Created by mac on 14-5-16.
//  Copyright (c) 2014年 hengtian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
